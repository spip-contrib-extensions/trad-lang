<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/actions');
include_spip('inc/editer');
include_spip('tradlang_fonctions');

function formulaires_editer_tradlang_module_charger($id_tradlang_module, $retour = '') {
	$config_fonc = null;
	$row = null;
	$hidden = null;
	$valeurs = formulaires_editer_objet_charger('tradlang_module', $id_tradlang_module, 0, '', $retour, $config_fonc, $row, $hidden);

	$modules = tradlang_getmodules_base();
	$modok = $modules[$valeurs['module']];
	foreach ($modok as $cle => $item) {
		if (strncmp($cle, 'langue_', 7) == 0) {
			$lgs[] .= substr($cle, 7);
		}
	}

	$valeurs['_langues'] = $lgs;
	$valeurs['codelangue'] = _request('codelangue');
	return $valeurs;
}

function formulaires_editer_tradlang_module_verifier($id_tradlang_module, $retour = '') {
	$lgs = [];
	$erreur = [];
	$module = sql_getfetsel('module', 'spip_tradlang_modules', 'id_tradlang_module = ' . (int) $id_tradlang_module);
	$modules = tradlang_getmodules_base();
	if (!isset($modules[$module])) {
		$erreur['module'] = _T('tradlang:erreur_module_inexistant');
	}
	$modok = $modules[$module];
	foreach ($modok as $cle => $item) {
		if (strncmp($cle, 'langue_', 7) == 0) {
			$lgs[] = substr($cle, 7);
		}
	}

	$nouvelle_langue = _request('codelangue');

	include_spip('inc/lang_liste');
	if ($nouvelle_langue) {
		if (in_array($nouvelle_langue, $lgs)) {
			$erreur['codelangue'] = _T('tradlang:erreur_code_langue_existant');
		} elseif (!array_key_exists($nouvelle_langue, $GLOBALS['codes_langues'])) {
			$erreur['codelangue'] = _T('tradlang:erreur_code_langue_invalide');
		}
	}

	$limite_trad = _request('limite_trad');
	if (!is_numeric($limite_trad) || ((int) $limite_trad < 0) || ((int) $limite_trad > 100)) {
		$erreur['limite_trad'] = _T('tradlang:erreur_limite_trad_invalide');
	}

	return $erreur;
}

function formulaires_editer_tradlang_module_traiter($id_tradlang_module, $retour = '') {
	$ret = [];
	$module = sql_getfetsel('nom_mod', 'spip_tradlang_modules', 'id_tradlang_module = ' . (int) $id_tradlang_module);
	if (_request('delete_module')) {
		$supprimer_module = charger_fonction('tradlang_supprimer_module', 'inc');
		$suppressions = $supprimer_module($id_tradlang_module);
		$ret['editable'] = false;
		if ((int) $suppressions && ($suppressions > 1)) {
			$ret['message_ok'] = _T('tradlang:message_suppression_module_trads_ok', ['nb' => $suppressions, 'module' => $module]);
		} else {
			$ret['message_ok'] = _T('tradlang:message_suppression_module_ok', ['module' => $module]);
		}
	} else {
		$res = sql_select('*', 'spip_tradlang_modules', 'id_tradlang_module = ' . (int) $id_tradlang_module);
		$modok = sql_fetch($res);
		$langue = _request('codelangue');

		$data = [
			'nom_mod' => _request('nom_mod') ?: $module,
			'lang_mere' => _request('lang_mere'),
			'texte' => _request('texte'),
			'priorite' => _request('priorite')
		];
		$limite_trad = _request('limite_trad') ?: 0;
		$data['limite_trad'] = $limite_trad;
		sql_updateq('spip_tradlang_modules', $data, 'id_tradlang_module = ' . (int) $id_tradlang_module);
		$ret['message_ok'] = _T('tradlang:message_module_updated', ['module' => $module]);

		if ($langue) {
			$sauvegarde = charger_fonction('tradlang_ajouter_code_langue', 'inc');
			$sauvegarde($modok, $langue);
			$ret['message_ok'] .= '<br />' . _T('tradlang:message_module_langue_ajoutee', ['module' => $module, 'langue' => $langue]);
		}
		$ret['editable'] = true;
		$ret['redirect'] = $retour;
	}
	return $ret;
}
