<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-tradlang?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'tradlang_description' => 'Mit diesem Plugin werden Spachdateien direkt in SPIP verwaltet.',
	'tradlang_slogan' => 'Sprachdateien verwalten'
);
