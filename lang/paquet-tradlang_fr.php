<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/trad-lang.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'tradlang_description' => 'Un plugin pour gérer directement les fichiers de langues depuis SPIP.',
	'tradlang_slogan' => 'Gérer les fichiers de langue'
);
