<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-tradlang?lang_cible=ru
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'tradlang_description' => 'Плагин позволяет управлять языковыми файлами из админки SPIP',
	'tradlang_slogan' => 'Управление языковыми файлами'
);
