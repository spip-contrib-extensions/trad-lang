# Changelog

## 4.1.0 (2023-07-11)

### Added

- !2 trad.spip.net#6 Gérer le nouveau format de fichiers de langues SPIP (retourne directement un array)
